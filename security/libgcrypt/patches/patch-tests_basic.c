--- tests/basic.c.orig	2022-03-14 19:35:04.000000000 +0900
+++ tests/basic.c	2023-12-15 01:24:42.000000000 +0900
@@ -218,11 +218,12 @@ progress_handler (void *cb_data, const c
 
 #if defined(__x86_64__) && defined(HAVE_GCC_INLINE_ASM_SSSE3) && \
     (defined(HAVE_COMPATIBLE_GCC_AMD64_PLATFORM_AS) || \
-     defined(HAVE_COMPATIBLE_GCC_WIN64_PLATFORM_AS))
+     defined(HAVE_COMPATIBLE_GCC_WIN64_PLATFORM_AS)) && \
+    defined(__SSE2__)
 # define CLUTTER_VECTOR_REGISTER_AMD64 1
 # define CLUTTER_VECTOR_REGISTER_COUNT 16
 #elif defined(__i386__) && SIZEOF_UNSIGNED_LONG == 4 && __GNUC__ >= 4 && \
-      defined(HAVE_GCC_INLINE_ASM_SSSE3)
+      defined(HAVE_GCC_INLINE_ASM_SSSE3) && defined(__SSE2__)
 # define CLUTTER_VECTOR_REGISTER_I386 1
 # define CLUTTER_VECTOR_REGISTER_COUNT 8
 #elif defined(HAVE_COMPATIBLE_GCC_AARCH64_PLATFORM_AS) && \
@@ -314,68 +315,41 @@ clutter_vector_registers(void)
   prepare_vector_data(data);
 
 #if defined(CLUTTER_VECTOR_REGISTER_AMD64)
-  asm volatile("movdqu %[data0], %%xmm0\n"
-	       "movdqu %[data1], %%xmm1\n"
-	       "movdqu %[data2], %%xmm2\n"
-	       "movdqu %[data3], %%xmm3\n"
-	       "movdqu %[data4], %%xmm4\n"
-	       "movdqu %[data5], %%xmm5\n"
-	       "movdqu %[data6], %%xmm6\n"
-	       "movdqu %[data7], %%xmm7\n"
-	       "movdqu %[data8], %%xmm8\n"
-	       "movdqu %[data9], %%xmm9\n"
-	       "movdqu %[data10], %%xmm10\n"
-	       "movdqu %[data11], %%xmm11\n"
-	       "movdqu %[data12], %%xmm12\n"
-	       "movdqu %[data13], %%xmm13\n"
-	       "movdqu %[data14], %%xmm14\n"
-	       "movdqu %[data15], %%xmm15\n"
+  asm volatile("movdqu (0 * 16)(%[data]), %%xmm0\n"
+	       "movdqu (1 * 16)(%[data]), %%xmm1\n"
+	       "movdqu (2 * 16)(%[data]), %%xmm2\n"
+	       "movdqu (3 * 16)(%[data]), %%xmm3\n"
+	       "movdqu (4 * 16)(%[data]), %%xmm4\n"
+	       "movdqu (5 * 16)(%[data]), %%xmm5\n"
+	       "movdqu (6 * 16)(%[data]), %%xmm6\n"
+	       "movdqu (7 * 16)(%[data]), %%xmm7\n"
+	       "movdqu (8 * 16)(%[data]), %%xmm8\n"
+	       "movdqu (9 * 16)(%[data]), %%xmm9\n"
+	       "movdqu (10 * 16)(%[data]), %%xmm10\n"
+	       "movdqu (11 * 16)(%[data]), %%xmm11\n"
+	       "movdqu (12 * 16)(%[data]), %%xmm12\n"
+	       "movdqu (13 * 16)(%[data]), %%xmm13\n"
+	       "movdqu (14 * 16)(%[data]), %%xmm14\n"
+	       "movdqu (15 * 16)(%[data]), %%xmm15\n"
 	      :
-	      : [data0] "m" (*data[0]),
-	        [data1] "m" (*data[1]),
-	        [data2] "m" (*data[2]),
-	        [data3] "m" (*data[3]),
-	        [data4] "m" (*data[4]),
-	        [data5] "m" (*data[5]),
-	        [data6] "m" (*data[6]),
-	        [data7] "m" (*data[7]),
-	        [data8] "m" (*data[8]),
-	        [data9] "m" (*data[9]),
-	        [data10] "m" (*data[10]),
-	        [data11] "m" (*data[11]),
-	        [data12] "m" (*data[12]),
-	        [data13] "m" (*data[13]),
-	        [data14] "m" (*data[14]),
-	        [data15] "m" (*data[15])
-	      : "memory"
-#ifdef __SSE2__
-	       ,"xmm0", "xmm1", "xmm2", "xmm3", "xmm4", "xmm5", "xmm6", "xmm7",
-	        "xmm8", "xmm9", "xmm10", "xmm11", "xmm12", "xmm13", "xmm14",
-	        "xmm15"
-#endif
+	      : [data] "r" (&data[0])
+	      : "memory", "xmm0", "xmm1", "xmm2", "xmm3", "xmm4", "xmm5",
+	        "xmm6", "xmm7", "xmm8", "xmm9", "xmm10", "xmm11", "xmm12",
+	        "xmm13", "xmm14", "xmm15"
 	      );
 #elif defined(CLUTTER_VECTOR_REGISTER_I386)
-  asm volatile("movdqu %[data0], %%xmm0\n"
-	       "movdqu %[data1], %%xmm1\n"
-	       "movdqu %[data2], %%xmm2\n"
-	       "movdqu %[data3], %%xmm3\n"
-	       "movdqu %[data4], %%xmm4\n"
-	       "movdqu %[data5], %%xmm5\n"
-	       "movdqu %[data6], %%xmm6\n"
-	       "movdqu %[data7], %%xmm7\n"
+  asm volatile("movdqu (0 * 16)(%[data]), %%xmm0\n"
+	       "movdqu (1 * 16)(%[data]), %%xmm1\n"
+	       "movdqu (2 * 16)(%[data]), %%xmm2\n"
+	       "movdqu (3 * 16)(%[data]), %%xmm3\n"
+	       "movdqu (4 * 16)(%[data]), %%xmm4\n"
+	       "movdqu (5 * 16)(%[data]), %%xmm5\n"
+	       "movdqu (6 * 16)(%[data]), %%xmm6\n"
+	       "movdqu (7 * 16)(%[data]), %%xmm7\n"
 	      :
-	      : [data0] "m" (*data[0]),
-	        [data1] "m" (*data[1]),
-	        [data2] "m" (*data[2]),
-	        [data3] "m" (*data[3]),
-	        [data4] "m" (*data[4]),
-	        [data5] "m" (*data[5]),
-	        [data6] "m" (*data[6]),
-	        [data7] "m" (*data[7])
-	      : "memory"
-#ifdef __SSE2__
-	       ,"xmm0", "xmm1", "xmm2", "xmm3", "xmm4", "xmm5", "xmm6", "xmm7"
-#endif
+	      : [data] "r" (&data[0])
+	      : "memory", "xmm0", "xmm1", "xmm2", "xmm3", "xmm4", "xmm5",
+	        "xmm6", "xmm7"
 	      );
 #elif defined(CLUTTER_VECTOR_REGISTER_AARCH64)
   asm volatile("mov x0, %[ptr]\n"
