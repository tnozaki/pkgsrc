$NetBSD: patch-src_OpenColorIO_CMakeLists.txt,v 1.2 2024/06/26 16:20:55 adam Exp $

Solaris linker does not support --exclude-libraries

--- src/OpenColorIO/CMakeLists.txt.orig	2024-01-30 07:11:35.000000000 +0000
+++ src/OpenColorIO/CMakeLists.txt
@@ -373,7 +373,7 @@ endif()
 
 set(CUSTOM_LINK_FLAGS ${PLATFORM_LINK_OPTIONS})
 
-if(UNIX AND NOT APPLE)
+if(UNIX AND NOT APPLE AND NOT CMAKE_HOST_SOLARIS)
     # Also hide all the symbols of dependent libraries to prevent clashes if
     # an app using this project is linked against other versions of our
     # dependencies.
