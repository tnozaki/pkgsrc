# $NetBSD: Makefile.common,v 1.13 2024/05/23 04:15:35 adam Exp $
# used by www/libcurl-gnutls/Makefile

DISTNAME=	curl-8.8.0
CATEGORIES=	www
MASTER_SITES=	https://curl.se/download/
EXTRACT_SUFX=	.tar.xz

MAINTAINER=	leot@NetBSD.org
HOMEPAGE=	https://curl.se/
COMMENT=	Client that groks URLs
# not completely, but near enough
LICENSE=	mit

DISTINFO_FILE?=	${.CURDIR}/../../www/curl/distinfo
PATCHDIR?=	${.CURDIR}/../../www/curl/patches

BUILD_DEFS+=	IPV6_READY

TEST_DEPENDS+=	${PYPKGPREFIX}-impacket-[0-9]*:../../net/py-impacket

PYTHON_VERSIONS_INCOMPATIBLE=	27

USE_TOOLS+=		nroff perl
USE_LIBTOOL=		yes
GNU_CONFIGURE=		yes
# Some systems use bundles instead of directories; this needs configuring
# because curl doesn't use default validation.
.if !empty(SSLCERTBUNDLE)
CONFIGURE_ARGS+=	--with-ca-bundle=${SSLCERTBUNDLE}
.else
CONFIGURE_ARGS+=	--with-ca-path=${SSLCERTS}
.endif
CONFIGURE_ARGS+=	--with-zlib=${BUILDLINK_PREFIX.zlib}
CONFIGURE_ARGS+=	--without-libpsl
PKGCONFIG_OVERRIDE=	libcurl.pc.in
TEST_TARGET=		check

SUBST_CLASSES+=		python
SUBST_STAGE.python=	pre-configure
SUBST_MESSAGE.python=	Adjust hard-coded python invocations
SUBST_FILES.python=	tests/data/test1451
SUBST_SED.python=	-e 's,python,${PYTHONBIN},g'

REPLACE_PERL+=		tests/*.pl tests/*/*.pl
REPLACE_PYTHON+=	tests/*.py

PYTHON_FOR_BUILD_ONLY=	test

.include "../../devel/gettext-lib/buildlink3.mk"
.include "../../devel/zlib/buildlink3.mk"
.include "../../lang/python/application.mk"
.include "../../mk/pthread.buildlink3.mk"
