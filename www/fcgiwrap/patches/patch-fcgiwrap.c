--- fcgiwrap.c.orig	2013-02-03 13:25:17.000000000 +0000
+++ fcgiwrap.c	2017-09-05 06:50:12.000000000 +0000
@@ -485,6 +485,9 @@ static void inherit_environment(void)
 	}
 }
 
+#if defined(__GNUC__)
+__attribute__((noreturn))
+#endif
 static void cgi_error(const char *message, const char *reason, const char *filename)
 {
 	printf("Status: %s\r\nContent-Type: text/plain\r\n\r\n%s\r\n",
