$NetBSD: patch-dev-requirements.txt,v 1.2 2024/05/20 10:10:17 wiz Exp $

Relax requirements.

--- dev-requirements.txt.orig	2024-03-15 09:05:18.000000000 +0000
+++ dev-requirements.txt
@@ -1,8 +1,8 @@
 grequests
 mock;python_version<"3.3"
 responses
-pytest==4.6.4
-pytest-runner==4.5.1
+pytest
+pytest-runner
 pytest-cov
 pytest-flake8
 # Latest releases of pyotp do not support python2.
