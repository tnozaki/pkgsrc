--- libstdc++-v3/config/os/bsd/netbsd/ctype_configure_char.cc.orig	2020-05-07 19:50:02.000000000 +0900
+++ libstdc++-v3/config/os/bsd/netbsd/ctype_configure_char.cc	2020-07-03 17:42:07.000000000 +0900
@@ -38,9 +38,19 @@ _GLIBCXX_BEGIN_NAMESPACE_VERSION
 
 // Information as gleaned from /usr/include/ctype.h
 
+#ifdef _CTYPE_A
+  extern "C" const unsigned short _C_ctype_tab_[];
+#else
+  extern "C" const unsigned char _C_ctype_[];
+#endif
+
   const ctype_base::mask*
   ctype<char>::classic_table() throw()
+#ifdef _CTYPE_A
   { return _C_ctype_tab_ + 1; }
+#else
+  { return _C_ctype_ + 1; }
+#endif
 
   ctype<char>::ctype(__c_locale, const mask* __table, bool __del,
 		     size_t __refs)
