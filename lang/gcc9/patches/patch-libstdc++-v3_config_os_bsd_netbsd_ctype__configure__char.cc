--- libstdc++-v3/config/os/bsd/netbsd/ctype_configure_char.cc.orig	2019-01-01 21:31:55.000000000 +0900
+++ libstdc++-v3/config/os/bsd/netbsd/ctype_configure_char.cc	2020-02-08 12:23:13.000000000 +0900
@@ -38,11 +38,19 @@ _GLIBCXX_BEGIN_NAMESPACE_VERSION
 
 // Information as gleaned from /usr/include/ctype.h
 
-  extern "C" const u_int8_t _C_ctype_[];
+#ifdef _CTYPE_A
+  extern "C" const unsigned short _C_ctype_tab_[];
+#else
+  extern "C" const unsigned char _C_ctype_[];
+#endif
 
   const ctype_base::mask*
   ctype<char>::classic_table() throw()
+#ifdef _CTYPE_A
+  { return _C_ctype_tab_ + 1; }
+#else
   { return _C_ctype_ + 1; }
+#endif
 
   ctype<char>::ctype(__c_locale, const mask* __table, bool __del,
 		     size_t __refs)
@@ -69,14 +77,14 @@ _GLIBCXX_BEGIN_NAMESPACE_VERSION
 
   char
   ctype<char>::do_toupper(char __c) const
-  { return ::toupper((int) __c); }
+  { return ::toupper((unsigned char)__c); }
 
   const char*
   ctype<char>::do_toupper(char* __low, const char* __high) const
   {
     while (__low < __high)
       {
-	*__low = ::toupper((int) *__low);
+	*__low = ::toupper((unsigned char)*__low);
 	++__low;
       }
     return __high;
@@ -84,14 +92,14 @@ _GLIBCXX_BEGIN_NAMESPACE_VERSION
 
   char
   ctype<char>::do_tolower(char __c) const
-  { return ::tolower((int) __c); }
+  { return ::tolower((unsigned char)__c); }
 
   const char*
   ctype<char>::do_tolower(char* __low, const char* __high) const
   {
     while (__low < __high)
       {
-	*__low = ::tolower((int) *__low);
+	*__low = ::tolower((unsigned char)*__low);
 	++__low;
       }
     return __high;
