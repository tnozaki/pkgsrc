--- libstdc++-v3/config/os/bsd/netbsd/ctype_inline.h.orig	2019-01-01 21:31:55.000000000 +0900
+++ libstdc++-v3/config/os/bsd/netbsd/ctype_inline.h	2020-02-08 12:22:31.000000000 +0900
@@ -48,7 +48,7 @@ _GLIBCXX_BEGIN_NAMESPACE_VERSION
   is(const char* __low, const char* __high, mask* __vec) const
   {
     while (__low < __high)
-      *__vec++ = _M_table[*__low++];
+      *__vec++ = _M_table[(unsigned char)*__low++];
     return __high;
   }
 
