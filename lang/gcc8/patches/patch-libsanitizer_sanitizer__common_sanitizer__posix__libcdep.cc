--- libsanitizer/sanitizer_common/sanitizer_posix_libcdep.cc.orig	2017-10-19 11:23:59.000000000 +0000
+++ libsanitizer/sanitizer_common/sanitizer_posix_libcdep.cc	2018-06-04 08:17:01.000000000 +0000
@@ -295,7 +295,7 @@ void PrepareForSandboxing(__sanitizer_sa
 #endif
 }
 
-#if SANITIZER_ANDROID || SANITIZER_GO
+#if SANITIZER_ANDROID || SANITIZER_GO || (SANITIZER_NETBSD && (__NetBSD_Version__ < 699002900))
 int GetNamedMappingFd(const char *name, uptr size) {
   return -1;
 }
