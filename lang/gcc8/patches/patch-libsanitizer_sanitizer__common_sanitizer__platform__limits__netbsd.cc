--- libsanitizer/sanitizer_common/sanitizer_platform_limits_netbsd.cc.orig	2018-06-04 07:55:25.000000000 +0000
+++ libsanitizer/sanitizer_common/sanitizer_platform_limits_netbsd.cc	2018-06-04 07:58:27.000000000 +0000
@@ -52,7 +52,9 @@
 #include <sys/time.h>
 #include <sys/timeb.h>
 #include <sys/times.h>
+#if (__NetBSD_Version__ >= 799002100)
 #include <sys/timespec.h>
+#endif
 #include <sys/timex.h>
 #include <sys/types.h>
 #include <sys/ucontext.h>
