--- libsanitizer/sanitizer_common/sanitizer_procmaps_freebsd.cc.orig	2018-06-04 08:19:17.000000000 +0000
+++ libsanitizer/sanitizer_common/sanitizer_procmaps_freebsd.cc	2018-06-04 08:34:36.000000000 +0000
@@ -9,7 +9,7 @@
 //===----------------------------------------------------------------------===//
 
 #include "sanitizer_platform.h"
-#if SANITIZER_FREEBSD || SANITIZER_NETBSD
+#if SANITIZER_FREEBSD || (SANITIZER_NETBSD && (__NetBSD_Version__ >= 799002200))
 #include "sanitizer_common.h"
 #if SANITIZER_FREEBSD
 #include "sanitizer_freebsd.h"
@@ -98,4 +98,4 @@ bool MemoryMappingLayout::Next(MemoryMap
 
 }  // namespace __sanitizer
 
-#endif  // SANITIZER_FREEBSD || SANITIZER_NETBSD
+#endif  // SANITIZER_FREEBSD || (SANITIZER_NETBSD && (__NetBSD_Version__ >= 799002200))
