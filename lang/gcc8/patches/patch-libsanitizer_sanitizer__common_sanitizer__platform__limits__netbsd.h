--- libsanitizer/sanitizer_common/sanitizer_platform_limits_netbsd.h.orig	2017-10-19 11:23:59.000000000 +0000
+++ libsanitizer/sanitizer_common/sanitizer_platform_limits_netbsd.h	2018-06-04 08:04:40.000000000 +0000
@@ -125,7 +125,9 @@ struct __sanitizer_ifaddrs {
   void *ifa_netmask;  // (struct sockaddr *)
   void *ifa_dstaddr;  // (struct sockaddr *)
   void *ifa_data;
+#if (__NetBSD_Version__ >= 799004000)
   unsigned int ifa_addrflags;
+#endif
 };
 
 typedef unsigned __sanitizer_pthread_key_t;
