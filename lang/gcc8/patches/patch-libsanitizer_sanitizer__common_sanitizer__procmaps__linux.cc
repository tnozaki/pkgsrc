--- libsanitizer/sanitizer_common/sanitizer_procmaps_linux.cc.orig	2017-10-19 11:23:59.000000000 +0000
+++ libsanitizer/sanitizer_common/sanitizer_procmaps_linux.cc	2018-06-04 08:34:11.000000000 +0000
@@ -9,7 +9,7 @@
 //===----------------------------------------------------------------------===//
 
 #include "sanitizer_platform.h"
-#if SANITIZER_LINUX
+#if SANITIZER_LINUX || (SANITIZER_NETBSD && (__NetBSD_Version__ < 799002200))
 #include "sanitizer_common.h"
 #include "sanitizer_procmaps.h"
 
@@ -72,4 +72,4 @@ bool MemoryMappingLayout::Next(MemoryMap
 
 }  // namespace __sanitizer
 
-#endif  // SANITIZER_LINUX
+#endif  // SANITIZER_LINUX || (SANITIZER_NETBSD && (__NetBSD_Version__ < 799002200))
