--- libsanitizer/sanitizer_common/sanitizer_linux.cc.orig	2017-10-19 11:23:59.000000000 +0000
+++ libsanitizer/sanitizer_common/sanitizer_linux.cc	2018-06-04 07:52:06.000000000 +0000
@@ -300,74 +300,88 @@ static void kernel_stat_to_stat(struct k
 }
 #endif
 
+#if SANITIZER_LINUX
+# if SANITIZER_USES_CANONICAL_LINUX_SYSCALLS
+#  define HAVE_NEWFSTATAT	1
+# elif SANITIZER_LINUX_USES_64BIT_SYSCALLS
+#  if SANITIZER_MIPS64
+#   define HAVE_KERNEL_STAT	1
+#  endif
+# else
+#  define HAVE_STAT64	1
+# endif
+#endif
+
+#if SANITIZER_FREEBSD
+# define HAVE_FSTATAT	1
+#endif
+
+#if SANITIZER_NETBSD
+# if __NetBSD_Version__ >= 699001600
+#  define HAVE_FSTATAT	1
+# endif
+#endif
+
 uptr internal_stat(const char *path, void *buf) {
-#if SANITIZER_FREEBSD || SANITIZER_NETBSD
+#if HAVE_FSTATAT
   return internal_syscall(SYSCALL(fstatat), AT_FDCWD, (uptr)path,
                           (uptr)buf, 0);
-#elif SANITIZER_USES_CANONICAL_LINUX_SYSCALLS
+#elif HAVE_NEWFSTATAT
   return internal_syscall(SYSCALL(newfstatat), AT_FDCWD, (uptr)path,
                           (uptr)buf, 0);
-#elif SANITIZER_LINUX_USES_64BIT_SYSCALLS
-# if defined(__mips64)
+#elif HAVE_KERNEL_STAT
   // For mips64, stat syscall fills buffer in the format of kernel_stat
   struct kernel_stat kbuf;
   int res = internal_syscall(SYSCALL(stat), path, &kbuf);
   kernel_stat_to_stat(&kbuf, (struct stat *)buf);
   return res;
-# else
-  return internal_syscall(SYSCALL(stat), (uptr)path, (uptr)buf);
-# endif
-#else
+#elif HAVE_STAT64
   struct stat64 buf64;
   int res = internal_syscall(SYSCALL(stat64), path, &buf64);
   stat64_to_stat(&buf64, (struct stat *)buf);
   return res;
+#else
+  return internal_syscall(SYSCALL(stat), (uptr)path, (uptr)buf);
 #endif
 }
 
 uptr internal_lstat(const char *path, void *buf) {
-#if SANITIZER_NETBSD
-  return internal_syscall(SYSCALL(lstat), path, buf);
-#elif SANITIZER_FREEBSD
+#if HAVE_FSTATAT
   return internal_syscall(SYSCALL(fstatat), AT_FDCWD, (uptr)path,
                           (uptr)buf, AT_SYMLINK_NOFOLLOW);
-#elif SANITIZER_USES_CANONICAL_LINUX_SYSCALLS
+#elif HAVE_NEWFSTATAT
   return internal_syscall(SYSCALL(newfstatat), AT_FDCWD, (uptr)path,
                          (uptr)buf, AT_SYMLINK_NOFOLLOW);
-#elif SANITIZER_LINUX_USES_64BIT_SYSCALLS
-# if SANITIZER_MIPS64
+#elif HAVE_KERNEL_STAT
   // For mips64, lstat syscall fills buffer in the format of kernel_stat
   struct kernel_stat kbuf;
   int res = internal_syscall(SYSCALL(lstat), path, &kbuf);
   kernel_stat_to_stat(&kbuf, (struct stat *)buf);
   return res;
-# else
-  return internal_syscall(SYSCALL(lstat), (uptr)path, (uptr)buf);
-# endif
-#else
+#elif HAVE_STAT64
   struct stat64 buf64;
   int res = internal_syscall(SYSCALL(lstat64), path, &buf64);
   stat64_to_stat(&buf64, (struct stat *)buf);
   return res;
+#else
+  return internal_syscall(SYSCALL(lstat), (uptr)path, (uptr)buf);
 #endif
 }
 
 uptr internal_fstat(fd_t fd, void *buf) {
-#if SANITIZER_FREEBSD || SANITIZER_LINUX_USES_64BIT_SYSCALLS || SANITIZER_NETBSD
-# if SANITIZER_MIPS64
+#if HAVE_KERNEL_STAT
   // For mips64, fstat syscall fills buffer in the format of kernel_stat
   struct kernel_stat kbuf;
   int res = internal_syscall(SYSCALL(fstat), fd, &kbuf);
   kernel_stat_to_stat(&kbuf, (struct stat *)buf);
   return res;
-# else
-  return internal_syscall(SYSCALL(fstat), fd, (uptr)buf);
-# endif
-#else
+#elif HAVE_STAT64
   struct stat64 buf64;
   int res = internal_syscall(SYSCALL(fstat64), fd, &buf64);
   stat64_to_stat(&buf64, (struct stat *)buf);
   return res;
+#else
+  return internal_syscall(SYSCALL(fstat), fd, (uptr)buf);
 #endif
 }
 
@@ -1012,7 +1026,7 @@ uptr GetPageSize() {
 }
 
 uptr ReadBinaryName(/*out*/char *buf, uptr buf_len) {
-#if SANITIZER_FREEBSD || SANITIZER_NETBSD
+#if SANITIZER_FREEBSD || (SANITIZER_NETBSD && (__NetBSD_Version__ >= 799002200))
 #if SANITIZER_FREEBSD
   const int Mib[4] = {CTL_KERN, KERN_PROC, KERN_PROC_PATHNAME, -1};
 #else
@@ -1072,10 +1086,10 @@ bool LibraryNameIs(const char *full_name
 // Call cb for each region mapped by map.
 void ForEachMappedRegion(link_map *map, void (*cb)(const void *, uptr)) {
   CHECK_NE(map, nullptr);
-#if !SANITIZER_FREEBSD
+#if SANITIZER_LINUX
   typedef ElfW(Phdr) Elf_Phdr;
   typedef ElfW(Ehdr) Elf_Ehdr;
-#endif  // !SANITIZER_FREEBSD
+#endif  // SANITIZER_LINUX
   char *base = (char *)map->l_addr;
   Elf_Ehdr *ehdr = (Elf_Ehdr *)base;
   char *phdrs = base + ehdr->e_phoff;
