--- libsanitizer/sanitizer_common/sanitizer_linux_libcdep.cc.orig	2018-06-04 07:44:15.000000000 +0000
+++ libsanitizer/sanitizer_common/sanitizer_linux_libcdep.cc	2018-06-04 07:49:47.000000000 +0000
@@ -428,9 +428,9 @@ void GetThreadStackAndTls(bool main, upt
 #endif
 }
 
-# if !SANITIZER_FREEBSD
+# if SANITIZER_LINUX
 typedef ElfW(Phdr) Elf_Phdr;
-# elif SANITIZER_WORDSIZE == 32 && __FreeBSD_version <= 902001  // v9.2
+# elif SANITIZER_FREEBSD && (__FreeBSD_version <= 902001) && SANITIZER_WORDSIZE == 32
 #  define Elf_Phdr XElf32_Phdr
 #  define dl_phdr_info xdl_phdr_info
 #  define dl_iterate_phdr(c, b) xdl_iterate_phdr((c), (b))
