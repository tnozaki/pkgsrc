$NetBSD: patch-libs_indicore_CMakeLists.txt,v 1.1 2024/05/03 11:17:14 wiz Exp $

Link against librt for shm_*

--- libs/indicore/CMakeLists.txt.orig	2024-05-03 10:33:03.807020284 +0000
+++ libs/indicore/CMakeLists.txt
@@ -51,6 +51,7 @@ if(UNIX)
     list(APPEND ${PROJECT_NAME}_SOURCES        
         sharedblob_parse.cpp
         shm_open_anon.c)
+    target_link_libraries(${PROJECT_NAME} rt)
 endif()
 
 target_compile_definitions(${PROJECT_NAME}
