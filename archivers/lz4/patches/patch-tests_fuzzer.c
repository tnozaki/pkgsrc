--- tests/fuzzer.c.orig	2020-11-16 13:59:35.000000000 +0900
+++ tests/fuzzer.c	2021-11-09 23:20:00.000000000 +0900
@@ -40,6 +40,9 @@
 #  undef  _GNU_SOURCE     /* in case it's already defined */
 #  define _GNU_SOURCE     /* MAP_ANONYMOUS even in -std=c99 mode */
 #  include <sys/mman.h>   /* mmap */
+#if !defined(MAP_ANONYMOUS) && defined(MAP_ANON)
+#define MAP_ANONYMOUS MAP_ANON
+#endif
 #endif
 #include "platform.h"   /* _CRT_SECURE_NO_WARNINGS */
 #include "util.h"       /* U32 */
