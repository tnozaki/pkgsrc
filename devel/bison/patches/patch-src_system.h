--- src/system.h.orig	2021-06-23 13:48:48.000000000 +0900
+++ src/system.h	2023-12-14 06:10:35.000000000 +0900
@@ -80,7 +80,7 @@ typedef size_t uintptr_t;
 
 // See https://lists.gnu.org/r/bug-bison/2019-10/msg00061.html
 // and https://trac.macports.org/ticket/59927.
-# if defined GCC_VERSION && 405 <= GCC_VERSION
+# if defined GCC_VERSION && 406 <= GCC_VERSION
 #  define IGNORE_TYPE_LIMITS_BEGIN \
      _Pragma ("GCC diagnostic push") \
      _Pragma ("GCC diagnostic ignored \"-Wtype-limits\"")
