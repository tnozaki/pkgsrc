--- autoscan.pl.orig	1999-01-05 22:28:42.000000000 +0900
+++ autoscan.pl	2021-03-05 13:49:17.000000000 +0900
@@ -364,7 +364,7 @@ sub output_headers
 	}
     }
     print CONF "AC_CHECK_HEADERS(" . join(' ', sort(@have_headers)) . ")\n"
-	if defined(@have_headers);
+	if (@have_headers);
 }
 
 sub output_identifiers
@@ -391,5 +391,5 @@ sub output_functions
 	}
     }
     print CONF "AC_CHECK_FUNCS(" . join(' ', sort(@have_funcs)) . ")\n"
-	if defined(@have_funcs);
+	if (@have_funcs);
 }
