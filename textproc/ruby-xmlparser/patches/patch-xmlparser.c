--- xmlparser.c.orig	2013-02-07 09:45:23.000000000 +0900
+++ xmlparser.c	2021-04-27 17:15:26.000000000 +0900
@@ -114,7 +114,7 @@ static ID id_skippedEntityHandler;
 #endif
 
 #define GET_PARSER(obj, parser) \
-  Data_Get_Struct(obj, XMLParser, parser)
+  Data_Get_Struct((VALUE)obj, XMLParser, parser)
 
 typedef struct _XMLParser {
   XML_Parser parser;
@@ -1780,7 +1780,7 @@ XMLParser_parse(int argc, VALUE* argv, V
       if (!ret) {
 	int err = XML_GetErrorCode(parser->parser);
 	const char* errStr = XML_ErrorString(err);
-	rb_raise(eXMLParserError, (char*)errStr);
+	rb_raise(eXMLParserError, "%s", errStr);
       }
     } while (!NIL_P(buf));
     return Qnil;
@@ -1803,7 +1803,7 @@ XMLParser_parse(int argc, VALUE* argv, V
       volatile VALUE encobj;
       volatile VALUE ustr;
       enc = rb_enc_find(parser->detectedEncoding);
-      if ((int)ENC_TO_ENCINDEX(enc) != rb_ascii8bit_encindex()) {
+      if (rb_enc_to_index(enc) != rb_ascii8bit_encindex()) {
         rb_enc_associate(str, enc);
         encobj = rb_enc_from_encoding(enc_xml);
         /* rb_str_encode may raises an exception */
@@ -1829,7 +1829,7 @@ XMLParser_parse(int argc, VALUE* argv, V
   if (!ret) {
     int err = XML_GetErrorCode(parser->parser);
     const char* errStr = XML_ErrorString(err);
-    rb_raise(eXMLParserError, (char*)errStr);
+    rb_raise(eXMLParserError, "%s", errStr);
   }
 
   return Qnil;
