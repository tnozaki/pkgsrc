--- i18n/number_decimalquantity.cpp.orig	2018-03-26 13:38:30.000000000 +0000
+++ i18n/number_decimalquantity.cpp	2018-05-28 02:21:50.000000000 +0000
@@ -391,7 +391,7 @@ void DecimalQuantity::_setToDoubleFast(d
         for (; i <= -22; i += 22) n /= 1e22;
         n /= DOUBLE_MULTIPLIERS[-i];
     }
-    auto result = static_cast<int64_t>(std::round(n));
+    auto result = static_cast<int64_t>(::round(n));
     if (result != 0) {
         _setToLong(result);
         scale -= fracLength;
