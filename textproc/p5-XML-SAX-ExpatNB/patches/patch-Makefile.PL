--- Makefile.PL.orig	2005-01-09 08:43:52.000000000 +0900
+++ Makefile.PL	2021-04-27 18:23:51.000000000 +0900
@@ -21,14 +21,17 @@ WriteMakefile(
 sub MY::install {
     package MY;
     my $script = shift->SUPER::install(@_);
-    $script =~ s/install :: (.*)$/install :: $1 install_sax_expat_nb/m;
-    $script .= <<"INSTALL";
+    unless ($ENV{'SKIP_SAX_INSTALL'}) {
+        $script =~ s/install :: (.*)$/install :: $1 install_sax_expat_nb/m;
+        $script .= <<"INSTALL";
 
 install_sax_expat_nb :
 \t\@\$(PERL) -MXML::SAX -e "XML::SAX->add_parser(q(XML::SAX::ExpatNB))->save_parsers()"
 
 INSTALL
-
+    } else {
+        warn "Note: 'make install' will skip XML::SAX::ExpatNB registration with XML::SAX!\n";
+    }
     return $script;
 }
 
